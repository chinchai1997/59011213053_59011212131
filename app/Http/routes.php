<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//
//Route::get('/', function () {
//    return view('index');
//});

Route::get('/', 'MovieController@index');

Route::get('welcome', function () {
    return view('welcome');
});

//Route::get('/post/{id}', 'PostsController@index');

Route::resource('posts', 'PostsController');

Route::get('/contact', 'PostsController@contact');

Route::get('/post/{id}', 'MovieController@showMovie');
Route::get('/new', function() {
    return view('review');
});
Route::any('/insert', 'CommentController@store');
Route::any('/update', 'MovieController@updateLike');
Route::any('/postNew', 'MovieController@store');

Route::get('/main', 'MainController@index');
Route::post('/main/checklogin', 'MainController@checklogin');
Route::get('main/successlogin', 'MainController@successlogin');
Route::get('main/logout', 'MainController@logout');
//Route::get('users', function () {
//   return 'Fan Hum yai';
//});
//
//Route::get('users/{id}', function($id) {
//   return "Your ID is ".$id;
//});

Route::auth();

Route::get('/home', 'HomeController@index');


Route::get('/{pageId}', function($pageId){

    return view('page',['pageId' => $pageId]);

});

Route::get('comments/{pageId}', 'CommentController@index');

Route::post('comments', 'CommentController@store');

Route::post('comments/{commentId}/{type}', 'CommentController@update');
