<?php

namespace App\Http\Controllers;

use App\Comment;
use App\MovieModel;
use Illuminate\Http\Request;
use DB;

use App\Http\Requests;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies['movies'] = MovieModel::all();
        return view('index', $movies);
    }

    public function showMovie($id) {
        $comments = Comment::all();
        $commentCount = Comment::count();
        $movie= MovieModel::where('id_movie', $id)->first();
        return view('post', compact('movie', 'comments', 'commentCount'));
    }

    public function updateLike(Request $request) {
        $id = $request->input('id');
//        echo "Seccess";
        $movie= MovieModel::where('id_movie', $id)->first();
        $like = $movie->like_movie;
        $like++;

        DB::table('movie')
            ->where('id_movie', $id)
            ->update(['like_movie' => $like]);
        $movies['movies'] = MovieModel::all();
        return view('index', $movies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->input('name_movie');
        $genre = $request->input('genre_movie');
        $post = $request->input('post_movie');
        $image = $request->input('image_movie');

        $data = array('name_movie' => $name, 'genre_movie' => $genre, 'post_movie' => $post, 'image_movie' => $image);
        DB::table('movie')->insert($data);

        $movies['movies'] = MovieModel::all();
        return view('index', $movies);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(MovieModel $movieModel)
    {
        return $movieModel;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MovieModel $movieModel)
    {
        if($movieModel->fill($request->all())->save()) {
            return true;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(MovieModel $movieModel)
    {
        if($movieModel->delete()) {
            return true;
        }
    }
}
