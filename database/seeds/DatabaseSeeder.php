<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        User::create([
//            'name' => 'Peradon Norasri',
//            'email' => 'disper1412@gmail.com',
//            'password' => 'eiei';
//        ]);
        Eloquent::unguard();
        $this->call(UsersTablesSeeder::class);
    }
}
