<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
    <title>Movie Reviewer</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic|Roboto:400,300,700' rel='stylesheet' type='text/css'>
    <link href={{ URL::asset('csss/style.css')}} rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" type="text/css" href={{ URL::asset('csss/magnific-popup.css')}}>
    <script type="text/javascript" src={{ URL::asset('jss/jquery.min.js')}}></script>
    <!-----768px-menu----->
    <link type="text/css" rel="stylesheet" href={{ URL::asset('csss/jquery.mmenu.all.css') }} />
    <script type="text/javascript" src={{ URL::asset('jss/jquery.mmenu.js') }}></script>
    <script type="text/javascript">
        //	The menu on the left
        $(function() {
            $('nav#menu-left').mmenu();
        });
    </script>
    <!-----//768px-menu----->
</head>
<body>

<div class="header_btm">
    <div class="wrap">
        <!------start-768px-menu---->
        <div id="page">
            <div id="header">
                <a class="navicon" href="#menu-left"> </a>
            </div>
            <nav id="menu-left">
                <ul>
                    <a class="navbar-brand" href="{{ url('/') }}">
                        Movie Review
                    </a>
                    <li><a href="about.html">About uss</a></li>
                    <li><a href="services.html">Service</a></li>
                    <li><a href="pages.html">Pages</a></li>
                    <li><a href="blog.html">Blog</a></li>
                    <li><a href="contact.html">Contact us</a></li>
                </ul>
            </nav>
        </div>
        <!------start-768px-menu---->
        <div class="header_sub">
            <div class="h_menu">
                <ul>
                    <li class="active"><a href={{url('/')}}>Movie Review</a></li>
                </ul>
            </div>
            <div class="h_search">
                <form>
                    <input type="text" value="" placeholder="search something...">
                    <input type="submit" value="">
                </form>
            </div>
            <div class="clear"> </div>
        </div>
    </div>
</div>
<!--------start-blog----------->
<div class="blog">
    <div class="main">
        <div class="wrap">
            <div class="single-top">
                <div class="wrapper_single">
                    <div class="wrapper_top">
                        <div class="grid_1 alpha">
                            <div class="date">
								<span>
									22
								</span>
                                Sep 2014
                            </div>
                        </div>
                        <div class="content span_2_of_single">
                            <h5 class="blog_title"><a href="bloginner.html">{{$movie->name_movie}}</a></h5>
                            <div class="links">
                                <h3 class="comments">By<a href="bloginner.html">&nbsp;Lorem Ipsum</a></h3>
                                <h3 class="comments"><a href="#">{{$commentCount}} comments</a></h3>
                                <h3 class="tags">Tags: <a href="#">{{$movie->genre_movie}}</a></h3>
                                <h3>Share</h3>
                                <h3>
                                    <div class="social_1">
                                        <ul>
                                            <li class="icon1_t"><a href="#"><span> </span></a></li>
                                            <li class="icon2_f"><a href="#"><span> </span></a></li>
                                        </ul>
                                    </div>
                                </h3>
                                <div class="clear"> </div>
                            </div>
                            <div class="content">
                                <div class="span-1-of-1">
                                    <a href="#"><img class="m_img"  src="images/sb1.jpg" alt=""/></a>
                                </div>
                                <div class="clear"> </div>
                            </div>
                            <figure>
                                <img src={{$movie->image_movie}} alt="Image" class="img-responsive">
                            </figure>
                            <div class="para para_2">
                                <p>{{$movie->post_movie}}</p>
                            </div>
                            <h6 class="text"><a href="#">Lorem Ipsum</a></h6>

                            <div class="comments">
                                <h4>{{$commentCount}} Comments</h4>
                                @foreach($comments as $comment)
                                    <div class="c_grid">
                                        <div class="person_1">
                                            <a href="#"><span><img src="http://icons.iconarchive.com/icons/papirus-team/papirus-status/256/avatar-default-icon.png"></span></a>
                                        </div>
                                        <div class="desc">
                                            <div class="c_sub_grid">
                                                <p><a href="#">Lorem Ipsum,10 Jan 2013 </a></p>
                                                <h6><a href="#">Reply</a></h6>
                                                <div class="clear"> </div>
                                            </div>
                                            <div class="para">
                                                <p>{{$comment->comment}}</p>
                                            </div>
                                        </div>
                                        <div class="clear"> </div>
                                    </div>
                                @endforeach
                            </div>
                            @if (Auth::guest())
                            @else
                            <div class="comments-area">
                                <h4>Leave a Comment</h4>
                                <form method="post" action="{{url('/insert')}}">
                                    <div class="text_area">
                                        {{ csrf_field() }}
                                        {{--<textarea onfocus="if(this.value == 'Message*') this.value='';" onblur="if(this.value == '') this.value='Message*';" name="body">Message*</textarea>--}}
                                        <input type="text" name="comment">
                                    </div>
                                    <div>
                                        <div class="button send_button">
                                            <input type="submit" value="Send">
                                        </div>
                                    </div>

                                </form>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="clear"> </div>
                </div>
                <div class="rsidebar span_1_of_3">
                    <div class="search_box">
                        <form>
                            <input type="text" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}"><input type="submit" value="">
                        </form>
                    </div>
                    <div class="social_2">
                        <h4>Follow Us</h4>
                        <ul>
                            <li class="facebook"><a href="#"><span> </span></a></li>
                            <li class="twitter"><a href="#"><span> </span></a></li>
                            <li class="google"><a href="#"><span> </span></a></li>
                        </ul>
                    </div>
                    <div class="email_box">
                        <form>
                            <div class="email">
                                <img src="https://scontent.fbkk5-5.fna.fbcdn.net/v/t1.15752-9/49027049_907256319664008_7822610032842768384_n.jpg?_nc_cat=100&_nc_ht=scontent.fbkk5-5.fna&oh=cdeaa87b5383ebe46c59c850866600ea&oe=5C9DFD59">
                                {{$movie->like_movie}}
                            </div>
                        </form>
                    </div>
                    @if (!Auth::guest())
                        <div class="comments-area2">
                            <form method="post" action="{{url('/update')}}">
                                <div class="text_area">
                                    {{ csrf_field() }}
                                    {{--<textarea onfocus="if(this.value == 'Message*') this.value='';" onblur="if(this.value == '') this.value='Message*';" name="body">Message*</textarea>--}}
                                    <input type="hidden" name="id" value={{$movie->id_movie}}>
                                </div>
                                <div>
                                    <div class="button send_buttonw">
                                        <input type="submit" value="Like">
                                    </div>
                                </div>

                            </form>
                        </div>
                    @endif

                    <div class="tags">
                        <h4>Tags</h4>
                        <ul>
                            <li><a href="#">{{$movie->genre_movie}}</a></li>
                            <div class="clear"> </div>
                        </ul>
                    </div>
                </div>
                <div class="clear"> </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>